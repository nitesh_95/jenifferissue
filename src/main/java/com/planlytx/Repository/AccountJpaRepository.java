package com.planlytx.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.planlytx.entities.Account;

@Repository
public interface AccountJpaRepository extends JpaRepository<Account, Long> {
	List<Account> findByUsername(String username);
}
