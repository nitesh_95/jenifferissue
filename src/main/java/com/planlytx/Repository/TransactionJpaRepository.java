package com.planlytx.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.planlytx.entities.Transaction;

@Repository
public interface TransactionJpaRepository extends JpaRepository<Transaction, Long> {
	List<Transaction> findByUsername(String username);
}
