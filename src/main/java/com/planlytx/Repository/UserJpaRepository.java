package com.planlytx.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.planlytx.entities.User;

@Repository
public interface UserJpaRepository extends JpaRepository<User, Long> {
	List<User> findByUsername(String username);
}
