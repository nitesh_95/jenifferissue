package com.planlytx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JenniferIssueApplication {

	public static void main(String[] args) {
		SpringApplication.run(JenniferIssueApplication.class, args);
	}

}
